# -*- coding: utf-8 -*-
"""
Created on Sun Mar  5 15:02:05 2023

@author: MCempa
"""
#BUBBLE SORT 

import time

def sort_list (list_name):
    n=len(list_name)
    while n>1:
        check=True
        for i in range(0,n-1):
            if list_name[i]>list_name[i+1]:
                 list_name[i], list_name[i+1]=list_name[i+1], list_name[i]
                 check=False
        if check==True:
            break
    print(f"Posortowany ciąg liczb:{list_name}")
    
def main (): 
    #list do sortowania
    list_A= []
    list_B= [1,2,5,3,1,7,9,1,12,83,1,5,3,2]
    list_C= [1,1,1,1,1,1,1,2,1,1,1]
    list_D= [2,2,2,2]
    list_E= [1,2]
    list_G= [2,1]    
    names=[list_A, list_B, list_C, list_D, list_E, list_G]
    
    for l in names:
        print(f"Ciąg liczb: {l}")
        sort_list(l) 
        print('________________')
        time.sleep(3)
    
main()